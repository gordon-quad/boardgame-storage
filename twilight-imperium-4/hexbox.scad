thickness=2.8;

hex_dia=116;
hex_depth=101;
hex_side=hex_dia/2;
box_height=110;
box_depth=hex_depth;
box_width=hex_dia+2*thickness;

side_height=box_height-thickness;
side_width=hex_side - thickness/2 - 0.5;

side_support_width=16;
side_support_top=10;

laser_adjust = 0.1;

module fingers(thickness, width, num, parity=false, tightness_adjust=laser_adjust) {
    finger_width = width / (2*num + 1);
    if (parity) {
        for (i = [0:num]) {
            translate([finger_width*2*i-tightness_adjust/2, 0]) square([finger_width + tightness_adjust, thickness]);
        }
    } else {
        for (i = [0:num-1]) {
            translate([finger_width + finger_width*2*i-tightness_adjust/2, 0]) square([finger_width + tightness_adjust, thickness]);
        }
    }
}

module base() {
    difference() {
        square([box_width, box_depth]);
        translate([box_width/2 - hex_dia/2 - 0.5, box_depth/2 - 0.5]) rotate([0,0,120]) translate([-side_width, 0]) {
 //                              #square([side_width, thickness]);
                               fingers(thickness, side_width, 4, tightness_adjust=-laser_adjust);
                               translate([side_width/2+thickness/2, thickness]) rotate([0,0,90]) fingers(thickness, side_support_width, 1, tightness_adjust=-laser_adjust);
        }
        translate([box_width/2 - hex_dia/2 - 0.5, box_depth/2 + 0.5]) rotate([0,0,60]) {
                               fingers(thickness, side_width, 4, tightness_adjust=-laser_adjust);
                               translate([side_width/2+thickness/2, thickness]) rotate([0,0,90]) fingers(thickness, side_support_width, 1, tightness_adjust=-laser_adjust);
        }
        translate([box_width/2 + hex_dia/2 + 0.5, box_depth/2 - 0.5]) rotate([0,0,-120]) {
                               fingers(thickness, side_width, 4, tightness_adjust=-laser_adjust);
                               translate([side_width/2+thickness/2, thickness]) rotate([0,0,90]) fingers(thickness, side_support_width, 1, tightness_adjust=-laser_adjust);
        }
        translate([box_width/2 + hex_dia/2 + 0.5, box_depth/2 + 0.5]) rotate([0,0,-60]) translate([-side_width, 0]) {
                               fingers(thickness, side_width, 4, tightness_adjust=-laser_adjust);
                               translate([side_width/2+thickness/2, thickness]) rotate([0,0,90]) fingers(thickness, side_support_width, 1, tightness_adjust=-laser_adjust);
        }
    }
//    translate([box_width/2, box_depth/2]) #circle(d=hex_dia, $fn=6);
}

module side() {
    difference() {
        union() {
            square([side_width, side_height]);
            translate([side_width, 0]) rotate([0,0,180]) fingers(thickness, side_width, 4);
        }
        translate([side_width/2+thickness/2, 0]) rotate([0,0,90]) fingers(thickness, side_height, 7, tightness_adjust=-laser_adjust);
    }
}

module side_a() {
    union() {
        side();
        rotate([0,0,90]) fingers(thickness/2, side_height, 7);
    }
}

module side_b() {
    union() {
        side();
        translate([side_width, side_height]) rotate([0,0,-90]) fingers(thickness/2, side_height, 7, true);
    }
}

module side_support() {
    union() {
            polygon([[0,0],
                     [side_support_width, 0],
                     [side_support_top, side_height],
                     [0, side_height]]);
            translate([side_support_width, 0]) rotate([0,0,180]) fingers(thickness, side_support_width, 1);
            rotate([0,0,90]) fingers(thickness, side_height, 7);
    }
}

translate([0, 0]) side();
translate([side_width+10, 0]) side();
translate([2*side_width+20, 0]) side();
translate([3*side_width+30, 0]) side();
translate([0, box_height+10]) base();
translate([box_width+10, box_height+10]) side_support();
translate([box_width+side_support_width+20, box_height+10]) side_support();
translate([box_width+side_support_width*2+30, box_height+10]) side_support();
translate([box_width+side_support_width*3+40, box_height+10]) side_support();
